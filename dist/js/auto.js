/**
 * Site : http:www.smarttutorials.net
 * @author muni
 */
	      
//adds extra table rows
var i=2;
$(".addmore").on('click',function(){
	html = '<tr>';
	html += '<td><input class="case" type="checkbox"/></td>';
	html += '<td><input type="text" data-type="productCode" name="itemNo[]" id="itemNo_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
	html += '<td><input type="text" data-type="productName" name="itemName[]" id="itemName_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
        html += '<td><input type="text" name="quantity[]" id="quantity_'+i+'" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '<td><input type="text" name="price[]" id="price_'+i+'" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '<td><input type="text" name="total[]" id="total_'+i+'" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '</tr>';
	$('table').append(html);
	i++;
});

//to check all checkboxes
$(document).on('change','#check_all',function(){
	$('input[class=case]:checkbox').prop("checked", $(this).is(':checked'));
});

//deletes the selected table rows
$(".delete").on('click', function() {
	$('.case:checkbox:checked').parents("tr").remove();
	$('#check_all').prop("checked", false); 
	calculateTotal();
});

//price change
$(document).on('change keyup blur','#discount',function(){
    calculateTotal();
});
//price change
$(document).on('change keyup blur','.changesNo',function(){
	id_arr = $(this).attr('id');
	id = id_arr.split("_");
        
	quantity = $('#quantity_'+id[1]).val();
	price = $('#price_'+id[1]).val();
	if( quantity!='' && price !='' ) $('#total_'+id[1]).val( (parseFloat(price)*parseFloat(quantity)).toFixed(2) );	
	calculateTotal();
});

$(document).on('change keyup blur','#tax',function(){
	calculateTotal();
});

//total price calculation 
function calculateTotal(){
	subTotal = 0 ; total = 0; 
	$('.totalLinePrice').each(function(){
		if($(this).val() != '' )subTotal += parseFloat( $(this).val() );
	});
        if($('#discount').val()!=''){
            subTotal = subTotal - $('#discount').val();
        }
        
	$('#subTotal').val( subTotal );
	tax = $('#tax').val();
	if(tax != '' && typeof(tax) != "undefined" ){
		taxAmount = subTotal * ( parseFloat(tax) /100 );
		$('#taxAmount').val(taxAmount.toFixed(2));
		total = subTotal + taxAmount;
	}else{
		$('#taxAmount').val(0);
		total = subTotal;
	}
	$('#totalAftertax').val( total.toFixed(2) );
        
        $('#texTotal').val('-'+numtothai(total.toFixed(2))+'.-');
        
	calculateAmountDue();
}

$(document).on('change keyup blur','#amountPaid',function(){
	calculateAmountDue();
});

//due amount calculation
function calculateAmountDue(){
	amountPaid = $('#amountPaid').val();
	total = $('#totalAftertax').val();
	if(amountPaid != '' && typeof(amountPaid) != "undefined" ){
		amountDue = parseFloat(total) - parseFloat( amountPaid );
		$('.amountDue').val( amountDue.toFixed(2) );
	}else{
		total = parseFloat(total).toFixed(2);
		$('.amountDue').val( total );
	}
}


//It restrict the non-numbers
var specialKeys = new Array();
specialKeys.push(8,46); //Backspace
function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    console.log( keyCode );
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    return ret;
}

function numtothaistring(num,type){
    var return_str = "";
    var txtnum1 = ['','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า'];
    var txtnum2 = ['','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน'];
    var num_arr = num.toString().split('');
    var count = num_arr.length;
    
    var indexpoint = count;
    
    num_arr.forEach(function(entry) {
        
        var txtnum2_int = parseInt(indexpoint-1);

        if(entry<=0){
            return_str += ''; 
        }else{
                if(txtnum2_int <=1 ){
                    switch(txtnum2_int){
                        case 1:
                            if(entry===2)
                                return_str += 'ยี่' + txtnum2[txtnum2_int];
                            else if(entry===1)
                               return_str += txtnum1[entry-1] + txtnum2[txtnum2_int];
                            else
                                return_str += txtnum1[entry] + txtnum2[txtnum2_int];
                            break;
                        case 0:
                            if(entry===1){
                                if(type==='stang')
                                   return_str += txtnum2[txtnum2_int+1];
                                else
                                    return_str += 'เอ็ด';
                            }else{
                                if(type==='stang'){
                                    if(entry===2){
                                        return_str += 'ยี่';                                        
                                    }else{
                                        return_str += txtnum1[entry];
                                    }
                                        return_str += txtnum2[txtnum2_int+1];
                                }else{
                                    return_str += txtnum1[entry] + txtnum2[txtnum2_int];
                                }
                            }
                            break;
                    }
                    
                }else{
                    return_str += txtnum1[entry] + txtnum2[txtnum2_int];
                }
        }
        indexpoint--;
    });
    
    return return_str;
}

function numtothai(num){
    var ret     = "";
    var num     = num.toString().replace(',','');
    var number  = num.split(".");
    var stang   = parseInt(number[1]);
    
    ret +=numtothaistring(number[0],'baht')+"บาท";
   
    if(stang > 0)
        ret += numtothaistring(stang,'stang')+"สตางค์";
    else
        ret += "ถ้วน";
    
    return ret ;
}
//datepicker
$(function () {
    $('#invoiceDate').datepicker({});
});