<?php   

defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_items extends CI_Model {

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }

    function getAll($order_by){
    	$this->db->order_by($order_by);
    	$query = $this->db->get('ordernamelists');
    	return $query;
    }
    
    function get($id){
    	//$this->db->order_by($order_by);
        $this->db->select('*');
        $this->db->join('customers', 'customers.id = quatations.customer_id', 'left');
        $query = $this->db->get_where('quatations',array('quatations.id' => $id));
        //$query = $this->db->get('quatations');
        
    	return $query;
    }
    
    function generateCode() {
        $this->db->select_max('id');
        $query = $this->db->get('ordernamelists');
        
        if(sizeof($query->result())>0){
            $no = $query->result()[0]->running_no+1;
        }else{
            $no ="1";
        }
        
        return $code = "ITEM-".str_pad($no, 3, '0', STR_PAD_LEFT);        
    }
    
}