<?php   

defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_customers extends CI_Model {

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }

    function getAll($order_by){
    	$this->db->order_by($order_by,"desc");
    	$query = $this->db->get('customers');
    	return $query;
    }
    
    function get($id){
        $this->db->select('*');
        $query = $this->db->get_where('customers',array('customers.id' => $id));
        
    	return $query;
    }
    
    function generateCode() {
        $year  = date('Y');
        $month = date('m');
        $this->db->select_max('running_no');
        $query = $this->db->get_where('customers',array('year' => $year, 'month'=>$month));
        
        if(sizeof($query->result())>0){
            $no = $query->result()[0]->running_no+1;
        }else{
            $no ="1";
        }
        
        return $code = "CT".$year.$month."-".str_pad($no, 5, '0', STR_PAD_LEFT);        
    }
    
}