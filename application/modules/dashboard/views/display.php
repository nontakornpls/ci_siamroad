<!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                  <a href="<?php echo base_url();?>quatations">
                <img class="img-circle" src="dist/img/qo-icon.png" alt="ใบเสนอราคา">
                  </a>
                <div class="info-box-content">
                  <span class="info-box-text">ใบเสนอราคา</span>
                  <span class="info-box-number">90<small>%</small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                <img class="img-circle" src="dist/img/iv-icon.png" alt="ใบแจ้งหนี้">
                <div class="info-box-content">
                  <span class="info-box-text">ใบแจ้งหนี้</span>
                  <span class="info-box-number">90<small>%</small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <img class="img-circle" src="dist/img/rt-icon.png" alt="ใบเสร็จ/ใบกำกับภาษี">
                <div class="info-box-content">
                  <span class="info-box-text">ใบเสร็จ/ใบกำกับภาษี</span>
                  <span class="info-box-number">90<small>%</small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <img class="img-circle" src="dist/img/sd-icon.png" alt="ตารางนัดหมายงาน">
                <div class="info-box-content">
                  <span class="info-box-text">ตารางนัดหมายงาน</span>
                  <span class="info-box-number">90<small></small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
         <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <img class="img-circle" src="dist/img/tf-icon.png" alt="ใบส่งมอบงาน">
                <div class="info-box-content">
                  <span class="info-box-text">ใบส่งมอบงาน</span>
                  <span class="info-box-number">90<small></small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
             <div class="info-box">
                <img class="img-circle" src="dist/img/wt-icon.png" alt="ใบรับประกันผลงาน">
                <div class="info-box-content">
                  <span class="info-box-text">ใบรับประกันผลงาน</span>
                  <span class="info-box-number">90<small></small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <img class="img-circle" src="dist/img/ct-icon.png" alt="ข้อมูลลูกค้า">
                <div class="info-box-content">
                  <span class="info-box-text">ข้อมูลลูกค้า</span>
                  <span class="info-box-number">90<small></small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <img class="img-circle" src="dist/img/rv-icon.png" alt="การเงิน">
                <div class="info-box-content">
                  <span class="info-box-text">การเงิน</span>
                  <span class="info-box-number">90<small></small></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>