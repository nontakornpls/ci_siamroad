<?php $result = $query->result(); ?>
<!-- Main content -->
        <section class="content">
            <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">ใบเสนอราคาเลขที่ : <?=$result[0]->code;?></h3>
              
            </div><!-- /.box-header -->
          
            <div class="box-body">
              <!-- START CUSTOM TABS -->
            <div class="row">
              <div class="col-md-12">
                  <div class="row pull-right">
                    <div class="form-group col-xs-12">
                    <label>วันที่ออก__<?=$result[0]->created_date;?>__</label>
                    </div>
                  </div>
              </div>
              <div class="col-md-12">
                        <div class="form-group col-xs-12">
                          <label>ชื่อลูกค้า : </label><?=$result[0]->customer_name;?>
                          <br/>
                          <label>ที่อยู่ : </label><?=$result[0]->address;?>
                          <br/>
                          <label>หมายเลขประจำตัวผู้เสียภาษี : </label><?=$result[0]->tax_id;?>
                        </div>
              </div><!-- /.col -->
              <div class="col-md-12">
                <div class="form-group col-xs-6">
                <h1>ยอดรวมสุทธิ</h1>
                </div>
                <div class="form-group col-xs-6">
                    <h1><?=  number_format($result[0]->netprice,2)?> ฿</h1>
                </div>
              </div><!-- /.col -->
              <div class="col-md-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title">รายละเอียดรายการดังนี้</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <table class="table table-condensed">
                    <tbody><tr>
                      <th style="width: 10px">#</th>
                      <th style="width: 40px">สินค้า</th>
                      <th>คำอธิบาย</th>
                      <th>จำนวน</th>
                      <th>ราคา</th> 
                      <th>ยอดรวมก่อนภาษี</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td>0214</td>
                      <td>ค่าแรง</td>
                      <td>100</td>
                      <td>30.00</td>
                      <td>3,000.00</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td align="right">ส่วนลด</td>
                      <td><span class="badge bg-green">0.00</span> บาท</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td align="right">ยอดรวมก่อนภาษี</td>
                      <td><span class="badge bg-green">0.00</span> บาท</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td align="right">ภาษีมูลค่าเพิ่ม</td>
                      <td><span class="badge bg-green">0.00</span> บาท</td>
                    </tr>
                    <tr>
                        <td colspan="4" align='center'><span class="badge bg-light-blue">(-สองพันบาทถ้วน-)</span></td>
                      <td align="right" >ยอดรวมสุทธิ</td>
                      <td><span class="badge bg-green">0.00</span> บาท</td>
                    </tr>
                    <tr>
                        <td colspan="6" align='right'>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="3" align='right'></td>
                      <td align='right'><button class="btn btn-block btn-danger">ส่งเมล์หาลูกค้า</button></td>
                      <td align='right'><button class="btn btn-block btn-success">พิมพ์เอกสาร</button></td>
                      <td align='right'><button class="btn btn-block btn-warning">แก้ไข</button></td>
                    </tr>
                    
                  </tbody></table>
                </div><!-- /.box-body -->
              </div>
              </div>
            </div>

          <!-- END CUSTOM TABS -->              
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->
        </section><!-- /.content -->
        
    <script type="text/javascript">
        $(function () {
        //Initialize Select2 Elements
          
        });
      
    </script>