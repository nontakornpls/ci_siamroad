<!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <div class="col-md-9">
                          <h3 class="box-title">รายการใบเสนอราคาทั้งหมด</h3>
                    </div>
                     <div class="col-md-3">
                         <a href="<?php echo base_url();?>quatations/create">
                    <button class="btn btn-block btn-primary btn-sm">+ สร้างใบเสนอราคา</button>
                         </a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>เลขที่</th>
                        <th>ชื่อลูกค้า</th>
                        <th>วันที่ออก</th>
                        <th>จำนวนตารางเมตร</th>
                        <th>มูลค่า (฿)</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach ($query->result() as $key=>$value) {?>
                        <tr>
                        <td><a href="detail/<?=$value->id;?>"><?=$value->code;?></a></td>
                        <td><?=$value->customer_name;?></td>
                        <td><?=$value->created_date;?></td>
                        <td><?=$value->area;?></td>
                        <td><?=$value->netprice;?></td>
                      </tr>
                            
                       <?php } ?>                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>เลขที่</th>
                        <th>ชื่อลูกค้า</th>
                        <th>วันที่ออก</th>
                        <th>จำนวนตารางเมตร</th>
                        <th>มูลค่า (฿)</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

        </section><!-- /.content -->
        <script type="text/javascript">
        $(function () {
            $("#example1").DataTable();
        });
        </script>