<!-- Main content -->
        <section class="content">
            <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">ใบเสนอราคาเลขที่ : <?=$running_no;?></h3>
              
            </div><!-- /.box-header -->
          
            <div class="box-body">
              <!-- START CUSTOM TABS -->
            <div class="row">
              <div class="col-md-12">
                  <div class="row pull-right">
                    <div class="form-group col-xs-12">
                    <label>วันที่ออก__<?=date('Y-m-d');?>__</label>
                    </div>
                  </div>
              </div>
              <div class="col-md-12">
                        <div class="form-group col-xs-12">
                            <label>ชื่อลูกค้า : </label>
                            <select class="form-control select2" style="width: 20%">
                                <option value="0">โปรดระบุ</option>
                                <?php 
                                foreach ($customers->result() as $key=>$value) {
                                
                                echo '<option value="'.$value->customer_id.'">'.$value->customer_name.'</option>';
                                
                                }?>
                              </select>
                          <br/>
                          <label>ที่อยู่ : </label>
                          <br/>
                          <label>หมายเลขประจำตัวผู้เสียภาษี : </label>
                        </div>
              </div><!-- /.col -->
              <div class="col-md-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title">รายละเอียดรายการดังนี้</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-condensed" >
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th style="width: 100px">สินค้า</th>
                                <th style="width: 400px">คำอธิบาย</th>
                                <th>จำนวน</th>
                                <th>ราคา</th> 
                                <th style="width: 200px">ยอดรวมก่อนภาษี</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                    <td><input class="case" type="checkbox"/></td>
                                    <td><input type="text" data-type="productCode" name="itemNo[]" id="itemNo_1" class="form-control autocomplete_txt" autocomplete="off"></td>
                                    <td><input type="text" data-type="productName" name="itemName[]" id="itemName_1" class="form-control autocomplete_txt" autocomplete="off"></td>
                                    <td><input type="text" name="quantity[]" id="quantity_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
                                    <td><input type="text" name="price[]" id="price_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
                                    <td><input type="text" name="total[]" id="total_1" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                            <td colspan="3">
                                <button class="btn btn-danger delete" type="button">- ลบรายการ</button>
                                <button class="btn btn-danger addmore">+เพิ่มรายการ</button></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                            <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right">ส่วนลด</td>
                            <td colspan="2"><span class="badge bg-green"><input type="text" class="form-control" id="discount" placeholder="ส่วนลด (ถ้ามี)" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></span> บาท</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right">ยอดรวมก่อนภาษี</td>
                            <td colspan="2"><span class="badge bg-green"><input type="text" class="form-control" id="subTotal" placeholder="Subtotal" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></span> บาท</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="right">ภาษีมูลค่าเพิ่ม</td>
                            <td colspan="2">
                                <span class="badge bg-green">
                                    <input type="text" class="form-control" id="tax" placeholder="Vat %" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
                                    <input type="text" class="form-control" id="taxAmount" readonly="readonly">
                                </span> บาท</td>
                          </tr>
                          <tr>
                              <td colspan="3" align='center'><span class="badge bg-light-blue"><input style="width: 500px;" type="text" class="form-control" id="texTotal"  readonly="readonly"></span></td>
                            <td align="right" >ยอดรวมสุทธิ</td>
                            <td colspan="2"><span class="badge bg-green"><input type="text" class="form-control" id="totalAftertax" placeholder="Total" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></span> บาท</td>
                          </tr>
                          <tr>
                              <td colspan="6" align='right'>&nbsp;</td>
                          </tr>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th style="width: 40px">สินค้า</th>
                                <th style="width: 400px">คำอธิบาย</th>
                                <th>จำนวน</th>
                                <th>ราคา</th> 
                                <th style="width: 200px">ยอดรวมก่อนภาษี</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
              </div>
              </div>
            </div>

          <!-- END CUSTOM TABS -->              
            </div><!-- /.box-body -->
            
          </div><!-- /.box -->
        </section><!-- /.content -->
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?php echo base_url();?>/dist/js/auto.js"></script>
    <script type="text/javascript">
        
        $(function () {
          $('.select2').select2();
          var items = <?php 
                        $itemlist = array();
                        foreach ($items->result() as $key=>$value) {
                            array_push($itemlist,$value->code."_".$value->id.':'.$value->ordername.':'.$value->priceperunit);
                        }
                                echo json_encode($itemlist);
                        ?>;
                                //autocomplete script
                        $(document).on('focus','.autocomplete_txt',function(){
                               type = $(this).data('type');

                               if(type =='productCode' )autoTypeNo=0;
                               if(type =='productName' )autoTypeNo=1; 	

                               $(this).autocomplete({
                                       source: items,
                                       autoFocus: true,	      	
                                       minLength: 0,
                                       select: function( event, ui ) {

                                               var names = ui.item.value.split(":");						
                                               id_arr = $(this).attr('id');
                                               id = id_arr.split("_");

                                               $('#itemNo_'+id[1]).val(names[0]);
                                               $('#itemName_'+id[1]).val(names[1]);
                                               $('#tax').val(7);
                                               $('#quantity_'+id[1]).val(100);
                                               $('#price_'+id[1]).val(names[2]);
                                               $('#total_'+id[1]).val( 1*names[2] );

                                               quantity = $('#quantity_'+id[1]).val();
                                               price = $('#price_'+id[1]).val();
                                               if( quantity!='' && price !='' ) $('#total_'+id[1]).val( (parseFloat(price)*parseFloat(quantity)).toFixed(2) );
                                               calculateTotal();
                                       }		      	
                               });
                       });
        });
        
        
      
    </script>