<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quatations extends MX_Controller {

	function index()
	{
		$this->load->model('mdl_quatations');
                $data['query'] = $this->mdl_quatations->getAll('id');
                    
                $data['module']     = "quatations";
                $data['view_file']  = "display";
                $data['title']      = "ใบเสนอราคา";
                echo Modules::run("template/admin",$data);
	}
        
        function create()
	{
		$this->load->model('mdl_quatations');
                $this->load->model('customers/mdl_customers');
                $this->load->model('items/mdl_items');
                
                
                $data['running_no'] = $this->mdl_quatations->generateCode();
                $data['customers'] = $this->mdl_customers->getAll('id');
                $data['items'] = $this->mdl_items->getAll('id');
                    
                
                $data['module']     = "quatations";
                $data['view_file']  = "create";
                $data['title']      = "ใบเสนอราคา";
                echo Modules::run("template/admin",$data);
	}
        
        function detail($id)
	{
		$this->load->model('mdl_quatations');
                $data['query'] = $this->mdl_quatations->get($id);
                    
                $data['module']     = "quatations";
                $data['view_file']  = "detail";
                $data['title']      = "ใบเสนอราคา";
                echo Modules::run("template/admin",$data);
	}
        
        
}
